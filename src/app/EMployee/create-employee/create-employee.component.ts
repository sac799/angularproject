import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EmployeeDataService } from 'src/app/service/employee-data.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {
  employeeForm = new FormGroup({
  });
  constructor(private emp: EmployeeDataService, private route: Router) { }

  ngOnInit() {
    this.employeeForm = new FormGroup({
      firstName: new FormControl("", [Validators.required]),
      lastName: new FormControl("", [Validators.required]),
      Age: new FormControl(null, [Validators.required]),
      Designation: new FormControl("", [Validators.required])
    })
  }

  onSubmit() {
    this.emp.addEmployeeData(this.employeeForm.value).subscribe((data) => {
      if (data) {
        window.alert("Data saved successfully...");
        this.route.navigate(['/employee-list']);
      }
    });

  }

}
