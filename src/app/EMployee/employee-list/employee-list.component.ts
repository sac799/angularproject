import { Component, OnInit } from '@angular/core';
import { EmployeeDataService } from 'src/app/service/employee-data.service';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
import { Employee } from 'src/app/employee';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  employeeList : Employee=[];
  constructor(private employeeData: EmployeeDataService) { }

  ngOnInit() {
    this.employeeData.getAllEmployees().subscribe((data)=>{
      this.employeeList =data
    })
    
  }

  loadEmployeeData(){
    this.employeeData.getAllEmployees().subscribe((data)=>{
      console.log(data);
    })
  }

  showEmployees(data){
    alert(JSON.stringify(data));
  }

  deleteEmp(id){
    this.employeeData.deleteEmployee(id).subscribe((data) => {
      if(data){
        alert("Deleted Successfully...");
      }
      
    })
  }
}
