import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router'
import { EmployeeDataService } from 'src/app/service/employee-data.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-update-employee',
  templateUrl: './update-employee.component.html',
  styleUrls: ['./update-employee.component.css']
})
export class UpdateEmployeeComponent implements OnInit {
  id;
  employeeForm: FormGroup;
  
  //read parameter 
  constructor(private activatedRoute: ActivatedRoute, private empService : EmployeeDataService, private router: Router) { }

  ngOnInit() {
    this.employeeForm = new FormGroup({
      firstName: new FormControl("", [Validators.required]),
      lastName: new FormControl("", [Validators.required]),
      Age: new FormControl(null, [Validators.required]),
      Designation: new FormControl("", [Validators.required])
    })

    this.id = this.activatedRoute.snapshot.params.id;        //To read parameters in activated route
    this.empService.getEmployeeData(this.id).subscribe((data) => {
      this.employeeForm.setValue({
        firstName: data.firstName,
        lastName: data.lastName,
        Age: data.Age,
        Designation: data.Designation
      })
    })

    

    alert(JSON.stringify(this.id));
  }
  // onUpdate(){
  //   this.empService.updateEmployee(this.id, this.employeeForm.value).subscribe((data) => {
  //     if(data){
  //       alert("Success...");
  //       this.ro
  //     }
  //   })
  // }
  
}
