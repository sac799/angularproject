import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeListComponent } from './EMployee/employee-list/employee-list.component';
import { CreateEmployeeComponent } from './EMployee/create-employee/create-employee.component';
import { UpdateEmployeeComponent } from './EMployee/update-employee/update-employee.component';
import { FormDemoComponent } from './form-demo/form-demo.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';


const routes: Routes = [
  {
    path: "employee-list", component:EmployeeListComponent
  },
  {
    path: "create-employee", component:CreateEmployeeComponent
  },
  {
    path: "update-employee/:id", component:UpdateEmployeeComponent  //Paramaterized routing
  },
  {
    path: "TemplateForm", component:FormDemoComponent
  },
  {
    path: "ReactiveForm", component:ReactiveFormComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
