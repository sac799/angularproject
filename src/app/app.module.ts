import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductComponent } from './product/product.component';
import { HomeDirective } from './home.directive';
import { PiPePipe } from './pi-pe.pipe';
import { ChildComponent } from './child/child.component';
import { ParentComponent } from './parent/parent.component';
import { OtherComponent } from './other/other.component';
import { FormDemoComponent } from './form-demo/form-demo.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import { CreateEmployeeComponent } from './EMployee/create-employee/create-employee.component';
import { UpdateEmployeeComponent } from './EMployee/update-employee/update-employee.component';
import { EmployeeListComponent } from './EMployee/employee-list/employee-list.component';
import { HttpClient } from 'selenium-webdriver/http';
import { EmployeeDataService } from './service/employee-data.service';

@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    HomeDirective,
    PiPePipe,
    ChildComponent,
    ParentComponent,
    OtherComponent,
    FormDemoComponent,
    ReactiveFormComponent,
    CreateEmployeeComponent,
    UpdateEmployeeComponent,
    EmployeeListComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
