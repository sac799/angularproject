import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HomeService } from '../home.service';
// import {DataServiceService} from '../data-service.service';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {
@Input() MSG;
@Output() msgEvent = new EventEmitter<string>();
childMsgFromChildWithOutput: string= "This message is from child with output";
childMsg : string = "This message is from Child";
msgFromService : string ;
msgFromOtherComp : string;
  constructor(private dataService : HomeService) { }

  ngOnInit() {
    this.dataService.getMessage.subscribe((data) => {
      this.msgFromService = data;
    })
  }

  //send msg to parent
  sendToParent(){
    this.msgEvent.emit(this.childMsgFromChildWithOutput);
  }

  //Service broadcasting
  changeMsgService(){
    this.dataService.changeMsg("change Msg Service from Child updated...");
  }
}
