import { Component, OnInit } from '@angular/core';
import { Form, NgForm } from '@angular/forms';

@Component({
  selector: 'app-form-demo',
  templateUrl: './form-demo.component.html',
  styleUrls: ['./form-demo.component.css']
})
export class FormDemoComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  ResetData(formData : NgForm){
    formData.resetForm();
  }

  onSubmit(data){
    alert("Form: "+JSON.stringify(data.value));
  }

}
