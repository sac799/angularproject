import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  
  private msgSource = new BehaviorSubject("Default msg from service");
  getMessage = this.msgSource.asObservable();
  constructor() { }

  changeMsg(msg:string){
    this.msgSource.next(msg);
  }

}
