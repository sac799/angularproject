export interface Employee {
    id:number,
    firstName:string,
    lastName:string,
    Age:number,
    Designation:string
}
