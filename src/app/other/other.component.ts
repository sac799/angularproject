import { Component, OnInit } from '@angular/core';
import { HomeService } from '../home.service';

@Component({
  selector: 'app-other',
  templateUrl: './other.component.html',
  styleUrls: ['./other.component.css']
})
export class OtherComponent implements OnInit {
  msgFromService : string;
  constructor(private dataService : HomeService) { }

  ngOnInit() {
    this.dataService.getMessage.subscribe((data) => {
      this.msgFromService = data;
    })
  }

  changeMsg(msg:string){
    this.dataService.changeMsg("This msg is from Other Component");
  }

}
