import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ChildComponent } from '../child/child.component';
import { HomeService } from '../home.service';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit, AfterViewInit {
@ViewChild(ChildComponent,{static: true}) child;
msg : string = "This message is from Parent Component...";
msgFromChild : string ;
msgFromChildWithOutput: string ;

  magFromService : string;
  msfFromOtherComp : string;

  constructor(private dataService : HomeService) { }
  
  ngOnInit() {
    this.dataService.getMessage.subscribe((data) => {
      this.magFromService = data;
  })
  }

  ngAfterViewInit(){
    this.msgFromChild = this.child.childMsg;
  }

  getMsgFromChild(cMsg : string){
    this.msgFromChildWithOutput = cMsg;
  }

  changeMsgServiceParent(){
    this.dataService.changeMsg("change Msg Service from Parent updated...");
  }
}
