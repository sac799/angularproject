import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'piPe'
})
export class PiPePipe implements PipeTransform {

  transform(value :string, Branch:string): string {
    if(Branch=="IT"){
      return "Er."+value;
    }
    else{
      return value;
    }
  }

}
