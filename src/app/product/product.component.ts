import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  items = 
  {
    name: "MacBook Air",
    price: 1000000
  }

  students = [
  {
    id : 1000,
    name : "Sachin",
    Branch: "IT",
  },
  {
    id : 1001,
    name : "John",
    Branch: "IT",
  },
  {
    id : 1002,
    name : "Mac",
    Branch: "ETC",
  },
  {
    id : 1003,
    name : "Steve",
    Branch: "IT",
  },
  {
    id : 1004,
    name : "Brett",
    Branch: "IT",
  }
  ]

  name1 : string ="Macbook Air";
  name2 : string ;
  constructor() { }

  ngOnInit() {
  }
  showProduct(data){
    // console.log(data);
    alert(JSON.stringify(data));
  }
}
