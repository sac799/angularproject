import { Component, OnInit  } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms'
@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.css']
})
export class ReactiveFormComponent implements OnInit {
  contactForm = new FormGroup({
  });
  constructor() { }

  ngOnInit() {
    this.contactForm = new FormGroup({
      firstname : new FormControl("",[Validators.required,Validators.minLength(10)]),
      lastname : new FormControl("",[Validators.required]),
      email : new FormControl("",[Validators.required, Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$")]),
      mobile : new FormControl(null,[Validators.required, Validators.pattern("[6789][0-9]{9}")])
    })
  }

  onSubmit(){
    alert("Form Values: "+JSON.stringify(this.contactForm.value))
  }

}
