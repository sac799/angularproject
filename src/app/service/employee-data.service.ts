import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Employee } from '../model/employee';
//Injecting service 
@Injectable({
  providedIn: 'root'
})
export class EmployeeDataService {
  apiURL: string = "http://localhost:3000";  //Defining API URL (JSON server)

  constructor(private http: HttpClient) { }

  //Method to utilize API

  //Get All Employees
  getAllEmployees(): Observable<Employee> {
    return this.http.get<Employee>(this.apiURL + '/Employee'); //GET method
  }

  //Service for store employee data
  addEmployeeData(employeeData): Observable<Employee> {
    return this.http.post<Employee>(this.apiURL + '/Employee', employeeData);
  }

  //get individual employee data
  getEmployeeData(id):Observable<Employee>{
    return this.http.get<Employee>(this.apiURL + '/Employee/' + id);
  }

  //update employee
  updateEmployee(id,employeeData):Observable<Employee>{
    return this.http.put<Employee>(this.apiURL + '/Employee/' + id, employeeData);
  }

  //Delete employee
  deleteEmployee(id):Observable<Employee>{
    return this.http.delete<Employee>(this.apiURL + '/Employee/' + id);
  }
}
